package model;

public final class Action {
    public double target_velocity_x;
    public double target_velocity_y;
    public double target_velocity_z;
    public double jump_speed;
    public boolean use_nitro;
	@Override
	public String toString() {
		return "Action [target_velocity_x=" + target_velocity_x + ", target_velocity_y=" + target_velocity_y
				+ ", target_velocity_z=" + target_velocity_z + ", jump_speed=" + jump_speed + ", use_nitro=" + use_nitro
				+ "]";
	}
}
