package model;

public final class Ball {
    public double x;
    public double y;
    public double z;
    public double velocity_x;
    public double velocity_y;
    public double velocity_z;
    public double radius;
	@Override
	public String toString() {
		return "Ball [x=" + x + ", y=" + y + ", z=" + z + ", velocity_x=" + velocity_x + ", velocity_y=" + velocity_y
				+ ", velocity_z=" + velocity_z + "]";
	}
    
}
