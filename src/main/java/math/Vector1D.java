package math;




public class Vector1D {
    /** Abscissa. */
    private final double x;

    /** Simple constructor.
     * Build a vector from its coordinates
     * @param x abscissa
     * @see #getX()
     */
    public Vector1D(double x) {
        this.x = x;
    }
    
    public double getNorm() {
        return Math.abs(x);
    }

	public double getX() {
		return x;
	}
	
    public Vector1D add(Vector1D v) {
        Vector1D v1 = (Vector1D) v;
        return new Vector1D(x + v1.getX());
    }
	
    public Vector1D normalize() {
        double s = getNorm();
        return scalarMultiply(1 / s);
    }
    public Vector1D scalarMultiply(double a) {
        return new Vector1D(a * x);
    }
}
