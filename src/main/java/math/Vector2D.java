package math;



public class Vector2D {

    /** Abscissa. */
    private final double x;

    /** Ordinate. */
    private final double y;

    /** Simple constructor.
     * Build a vector from its coordinates
     * @param x abscissa
     * @param y ordinate
     * @see #getX()
     * @see #getY()
     */
    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
    public double distance(Vector2D v) {
        final Vector2D v3 = v;
        final double dx = v3.x - x;
        final double dy = v3.y - y;
        return Math.sqrt(dx * dx + dy * dy);
    }
    public double getNorm() {
        return Math.sqrt (x * x + y * y);
    }
    
    public Vector2D subtract(final Vector2D v) {
        final Vector2D v3 = v;
        return new Vector2D(x - v3.x, y - v3.y);
    }
    
    public Vector2D normalize() {
        double s = getNorm();
        return scalarMultiply(1 / s);
    }
    
    /** {@inheritDoc} */
    public Vector2D scalarMultiply(double a) {
        return new Vector2D(a * x, a * y);
    }
    
	@Override
	public String toString() {
		return "Vector2D [x=" + x + ", y=" + y + "]";
	}

	public Vector2D add(final Vector2D v) {
        final Vector2D v3 = v;
        return new Vector2D(x + v3.x, y + v3.y);
    }
    
    
    
    
}
