package math;




import static java.lang.Math.min;

import static java.lang.Math.max;

public class ClampMath {

//	function clamp(x, min, max):
//    if (x < min) then
//        x = min;
//    else if (x > max) then
//        x = max;
//    return x;
	
	public static Vector3D clamp(Vector3D val, Vector3D min, Vector3D max) {
		if (val.getNorm()<=min.getNorm()) val = min;
		else val = max;
		return val;		
	}
	public static Vector3D clamp(Vector3D val, Vector3D min, double max) {		
		if (val.getNorm()<=min.getNorm()) val = min;
		else val = val.normalize().scalarMultiply(max);
		return val;		
	}
	public static Vector3D clamp(Vector3D val, Vector3D max) {
	    return clamp(new Vector3D(0, 0, 0), val, max);
	}
	public static Vector3D clamp(Vector3D val, double max) {
	    return clamp(new Vector3D(0, 0, 0), val, max);
	}
	public static double clamp(double val, double min, double max) {
	    return max(min, min(max, val));
	}
	public static double clamp(double val, double max) {
	    return clamp(val, 0, max);
	}
}
