package math;




public class Vector3D {
    /** Abscissa. */
    private final double x;

    /** Ordinate. */
    private final double y;

    /** Height. */
    private final double z;
    /** Simple constructor.
     * Build a vector from its coordinates
     * @param x abscissa
     * @param y ordinate
     * @param z height
     * @see #getX()
     * @see #getY()
     * @see #getZ()
     */
    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getZ() {
		return z;
	}

    /** {@inheritDoc} */
    public Vector3D scalarMultiply(double a) {
        return new Vector3D(a * x, a * y, a * z);
    }
    
    /** {@inheritDoc} */
    public Vector3D add(final Vector3D v) {
        final Vector3D v3 = v;
        return new Vector3D(x + v3.x, y + v3.y, z + v3.z);
    }
    
    public Vector3D subtract(final Vector3D v) {
        final Vector3D v3 = (Vector3D) v;
        return new Vector3D(x - v3.x, y - v3.y, z - v3.z);
    }
    
    public double getNorm() {
        // there are no cancellation problems here, so we use the straightforward formula
        return Math.sqrt (x * x + y * y + z * z);
    }
    
    public Vector3D normalize() {
        double s = getNorm();
        return scalarMultiply(1 / s);
    }
    
    public double distance(Vector3D v) {
        final Vector3D v3 = (Vector3D) v;
        final double dx = v3.x - x;
        final double dy = v3.y - y;
        final double dz = v3.z - z;
        return Math.sqrt(dx * dx + dy * dy + dz * dz);
    }
	@Override
	public String toString() {
		return "[x=" + x + ", y=" + y + ", z=" + z + "]";
	}
    
    
}
