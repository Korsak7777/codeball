package math;






import model.Action;

public class SpeedVector {

	 
	private double target_velocity_x;
	private double target_velocity_y;
	private double target_velocity_z;
	
	public SpeedVector(double target_velocity_x, double target_velocity_y, double target_velocity_z) {
		this.target_velocity_x = target_velocity_x;
		this.target_velocity_y = target_velocity_y;
		this.target_velocity_z = target_velocity_z;
	}
	
	public Action toAction() {
		Action action = new Action();
		action.target_velocity_x = target_velocity_x;
		action.target_velocity_y = target_velocity_y;
		action.target_velocity_z = target_velocity_z;

		return action;
	}
	
		
	public double getTarget_velocity_x() {
		return target_velocity_x;
	}
	public void setTarget_velocity_x(double target_velocity_x) {
		this.target_velocity_x = target_velocity_x;
	}
	public double getTarget_velocity_y() {
		return target_velocity_y;
	}
	public void setTarget_velocity_y(double target_velocity_y) {
		this.target_velocity_y = target_velocity_y;
	}
	public double getTarget_velocity_z() {
		return target_velocity_z;
	}
	public void setTarget_velocity_z(double target_velocity_z) {
		this.target_velocity_z = target_velocity_z;
	}
	
}
