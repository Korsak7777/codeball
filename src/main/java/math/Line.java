package math;




import static java.lang.Math.hypot;

import static java.lang.Math.atan2;
import static java.lang.Math.PI;

public class Line {
	
    /** Angle with respect to the abscissa axis. */
    private double angle;

    /** Cosine of the line angle. */
    private double cos;

    /** Sine of the line angle. */
    private double sin;

    /** Offset of the frame origin. */
    private double originOffset;

    /** Tolerance below which points are considered identical. */
    private final double tolerance;

    /** Reverse line. */
    private Line reverse;
    
    /** Build a line from two points.
     * <p>The line is oriented from p1 to p2</p>
     * @param p1 first point
     * @param p2 second point
     * @param tolerance tolerance below which points are considered identical
     * @since 3.3
     */
    public Line(final Vector2D p1, final Vector2D p2, final double tolerance) {
        reset(p1, p2);
        this.tolerance = tolerance;
    }
    
    /** Reset the instance as if built from two points.
     * <p>The line is oriented from p1 to p2</p>
     * @param p1 first point
     * @param p2 second point
     */
    public void reset(final Vector2D p1, final Vector2D p2) {
        unlinkReverse();
        final double dx = p2.getX() - p1.getX();
        final double dy = p2.getY() - p1.getY();
        final double d = hypot(dx, dy);
        if (d == 0.0) {
            angle        = 0.0;
            cos          = 1.0;
            sin          = 0.0;
            originOffset = p1.getY();
        } else {
            angle        = PI + atan2(-dy, -dx);
            cos          = dx / d;
            sin          = dy / d;
            originOffset = linearCombination(p2.getX(), p1.getY(), -p1.getX(), p2.getY()) / d;
        }
    }
    
    /** Unset the link between an instance and its reverse.
     */
    private void unlinkReverse() {
        if (reverse != null) {
            reverse.reverse = null;
        }
        reverse = null;
    }
    
    /** Transform a space point into a sub-space point.
     * @param vector n-dimension point of the space
     * @return (n-1)-dimension point of the sub-space corresponding to
     * the specified space point
     */
    public Vector1D toSubSpace(Vector2D vector) {
        return new Vector1D(linearCombination(cos, vector.getX(), sin, vector.getY()));
    }
    
    public Vector2D toSpace(Vector1D vector) {
        final double abscissa = vector.getX();
        return new Vector2D(linearCombination(abscissa, cos, -originOffset, sin),
                linearCombination(abscissa, sin,  originOffset, cos));
    }
    
    public static double linearCombination(final double a1, final double b1,
            final double a2, final double b2) {

// the code below is split in many additions/subtractions that may
// appear redundant. However, they should NOT be simplified, as they
// use IEEE754 floating point arithmetic rounding properties.
// The variable naming conventions are that xyzHigh contains the most significant
// bits of xyz and xyzLow contains its least significant bits. So theoretically
// xyz is the sum xyzHigh + xyzLow, but in many cases below, this sum cannot
// be represented in only one double precision number so we preserve two numbers
// to hold it as long as we can, combining the high and low order bits together
// only at the end, after cancellation may have occurred on high order bits

// split a1 and b1 as one 26 bits number and one 27 bits number
final double a1High     = Double.longBitsToDouble(Double.doubleToRawLongBits(a1) & ((-1L) << 27));
final double a1Low      = a1 - a1High;
final double b1High     = Double.longBitsToDouble(Double.doubleToRawLongBits(b1) & ((-1L) << 27));
final double b1Low      = b1 - b1High;

// accurate multiplication a1 * b1
final double prod1High  = a1 * b1;
final double prod1Low   = a1Low * b1Low - (((prod1High - a1High * b1High) - a1Low * b1High) - a1High * b1Low);

// split a2 and b2 as one 26 bits number and one 27 bits number
final double a2High     = Double.longBitsToDouble(Double.doubleToRawLongBits(a2) & ((-1L) << 27));
final double a2Low      = a2 - a2High;
final double b2High     = Double.longBitsToDouble(Double.doubleToRawLongBits(b2) & ((-1L) << 27));
final double b2Low      = b2 - b2High;

// accurate multiplication a2 * b2
final double prod2High  = a2 * b2;
final double prod2Low   = a2Low * b2Low - (((prod2High - a2High * b2High) - a2Low * b2High) - a2High * b2Low);

// accurate addition a1 * b1 + a2 * b2
final double s12High    = prod1High + prod2High;
final double s12Prime   = s12High - prod2High;
final double s12Low     = (prod2High - (s12High - s12Prime)) + (prod1High - s12Prime);

// final rounding, s12 may have suffered many cancellations, we try
// to recover some bits from the extra words we have saved up to now
double result = s12High + (prod1Low + prod2Low + s12Low);

if (Double.isNaN(result)) {
// either we have split infinite numbers or some coefficients were NaNs,
// just rely on the naive implementation and let IEEE754 handle this
result = a1 * b1 + a2 * b2;
}

return result;
}
    
    
}
