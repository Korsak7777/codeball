package newModel;

import math.Vector3D;


import model.Robot;

public class NewRobot extends Entity{
    public int id;
    public int player_id;
    public boolean is_teammate;
    public double nitro_amount;
    public boolean touch;
    public Double touch_normal_x;
    public Double touch_normal_y;
    public Double touch_normal_z;    
    
	public NewRobot(Robot me) {
		this.id = me.id;
		this.player_id = me.player_id;
		this.is_teammate = me.is_teammate;
		this.x = me.x;
		this.y = me.y;
		this.z = me.z;
		this.velocity_x = me.velocity_x;
		this.velocity_y = me.velocity_y;
		this.velocity_z = me.velocity_z;
		this.radius = me.radius;
		this.nitro_amount = me.nitro_amount;
		this.touch = me.touch;
		this.touch_normal_x = me.touch_normal_x;
		this.touch_normal_y = me.touch_normal_y;
		this.touch_normal_z = me.touch_normal_z;	
		}

	public NewRobot(Vector3D needSpeed, Robot me) {
		this.id = me.id;
		this.player_id = me.player_id;
		this.is_teammate = me.is_teammate;
		this.x = me.x;
		this.y = me.y;
		this.z = me.z;
		this.velocity_x = needSpeed.getX();
		this.velocity_y = needSpeed.getY();
		this.velocity_z = needSpeed.getZ();
		this.radius = me.radius;
		this.nitro_amount = me.nitro_amount;
		this.touch = me.touch;
		this.touch_normal_x = me.touch_normal_x;
		this.touch_normal_y = me.touch_normal_y;
		this.touch_normal_z = me.touch_normal_z;	
		}
	
	public NewRobot(Vector3D needPosition, Vector3D needSpeed, Robot me) {
		super(needPosition, needSpeed);
		this.id = me.id;
		this.player_id = me.player_id;
		this.is_teammate = me.is_teammate;
		this.radius = me.radius;
		this.nitro_amount = me.nitro_amount;
		this.touch = me.touch;
		this.touch_normal_x = me.touch_normal_x;
		this.touch_normal_y = me.touch_normal_y;
		this.touch_normal_z = me.touch_normal_z;	}
	
	public NewRobot() {
	}
	
	public NewRobot(Vector3D pointVector, Vector3D speedVector, NewRobot me) {
		super(pointVector, speedVector);
		this.id = me.id;
		this.player_id = me.player_id;
		this.is_teammate = me.is_teammate;
		this.radius = me.radius;
		this.nitro_amount = me.nitro_amount;
		this.touch = me.touch;
		this.touch_normal_x = me.touch_normal_x;
		this.touch_normal_y = me.touch_normal_y;
		this.touch_normal_z = me.touch_normal_z;		
	}

	public Vector3D getTouchNormal() {
		return new Vector3D(touch_normal_x, touch_normal_y, touch_normal_z);
	}
	public void setTouchNormal(Vector3D touchNormal) {
		this.touch_normal_x = touchNormal.getX();
		this.touch_normal_y = touchNormal.getY();
		this.touch_normal_z = touchNormal.getZ();
	}

	@Override
	public String toString() {
		return "Robot [id=" + id + ", x=" + x + ", y=" + y + ", z=" + z + ", velocity_x=" + velocity_x + ", velocity_y="
				+ velocity_y + ", velocity_z=" + velocity_z + ", touch=" + touch + "]";
	}
}
