package newModel;




import model.Rules;

public class NewRules {
	public double ROBOT_MAX_JUMP_SPEED_IN_TICKS;
    public double ROBOT_ACCELERATION_IN_TICKS;
    public double ROBOT_NITRO_ACCELERATION_IN_TICKS;
    public double ROBOT_MAX_GROUND_SPEED_IN_TICKS;
    
    public double MAX_ENTITY_SPEED_IN_TICKS;

    public double GRAVITY_IN_TICKS;

	public NewRules(Rules rules) {
		ROBOT_MAX_JUMP_SPEED_IN_TICKS = rules.ROBOT_MAX_JUMP_SPEED * ((double)1/rules.TICKS_PER_SECOND);
		ROBOT_ACCELERATION_IN_TICKS = rules.ROBOT_ACCELERATION * ((double)1/(rules.TICKS_PER_SECOND * rules.TICKS_PER_SECOND));
		ROBOT_NITRO_ACCELERATION_IN_TICKS = rules.ROBOT_NITRO_ACCELERATION * ((double)1/(rules.TICKS_PER_SECOND * rules.TICKS_PER_SECOND));;
		ROBOT_MAX_GROUND_SPEED_IN_TICKS = rules.ROBOT_MAX_GROUND_SPEED * ((double)1/rules.TICKS_PER_SECOND);
		MAX_ENTITY_SPEED_IN_TICKS = rules.MAX_ENTITY_SPEED * ((double)1/rules.TICKS_PER_SECOND);
		GRAVITY_IN_TICKS = rules.GRAVITY * ((double)1/(rules.TICKS_PER_SECOND * rules.TICKS_PER_SECOND));
	}

}
