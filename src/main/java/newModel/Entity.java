package newModel;

import math.Vector2D;
import math.Vector3D;


import model.Action;

public class Entity {
	
    public double x;
    public double y;
    public double z;
    public double velocity_x;
    public double velocity_y;
    public double velocity_z;
    public double mass;
    public double radius;
    
    public Action action;
    public int tick;
    
    public Entity() {
    }

	public Entity(Vector3D pointVector, Vector3D speedVector) {
		this.x = pointVector.getX();
		this.y = pointVector.getY();
		this.z = pointVector.getZ();
		this.velocity_x = speedVector.getX();
		this.velocity_y = speedVector.getY();
		this.velocity_z = speedVector.getZ();
		this.mass = getMass();
		this.radius = getRadius();
	}
	
	public Entity(Vector3D pointVector, Vector3D speedVector, Entity entity) {
		this.x = pointVector.getX();
		this.y = pointVector.getY();
		this.z = pointVector.getZ();
		this.velocity_x = speedVector.getX();
		this.velocity_y = speedVector.getY();
		this.velocity_z = speedVector.getZ();
		this.mass = entity.mass;
		this.radius = entity.radius;
	}
		
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getZ() {
		return z;
	}
	public double getVelosityX() {
		return velocity_x;
	}
	public double getVelosityY() {
		return velocity_y;
	}
	public double getVelosityZ() {
		return velocity_z;
	}
	public Vector3D getPosition() {
		return new Vector3D(getX(), getY(), getZ());
	}
	public Vector2D getPositionXZ() {
		return new Vector2D(getX(), getZ());
	}
	public Vector3D getVelosity() {
		return new Vector3D(getVelosityX(), getVelosityY(), getVelosityZ());
	}
	public Vector2D getVelosityXZ() {
		return new Vector2D(getVelosityX(), getVelosityZ());
	}
	
	public double getVelosityXperTick() {
		return velocity_x*(1.0/60.0);
	}
	public double getVelosityYperTick() {
		return velocity_y*(1.0/60.0);
	}
	public double getVelosityZperTick() {
		return velocity_z*(1.0/60.0);
	}
	public Vector3D getVelosityPerTick() {
		return new Vector3D(getVelosityXperTick(), getVelosityYperTick(), getVelosityZperTick());
	}
	public Vector2D getVelosityXZperTick() {
		return new Vector2D(getVelosityXperTick(), getVelosityZperTick());
	}	
	
	public double getRadius() {
		return radius;
	}
	
	public double getMass() {
		return mass;
	}
}
