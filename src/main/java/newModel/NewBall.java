package newModel;

import math.Vector3D;


import model.Ball;

public final class NewBall extends Entity{
    
    public NewBall() {
    }
    
    public NewBall(Ball ball) {
    	this.x = ball.x;
    	this.y = ball.y;
    	this.z = ball.z;
    	this.velocity_x = ball.velocity_x;
    	this.velocity_y = ball.velocity_y;
    	this.velocity_z = ball.velocity_z;
    	this.radius = ball.radius;
    }
    
	public NewBall(Vector3D pointVector, Vector3D speedVector, double radius) {
		super(pointVector, speedVector);
		this.radius = radius;
	}

	public NewBall(Vector3D pointVector, Vector3D speedVector, NewBall startEntity) {
		super(pointVector, speedVector);
		this.radius = startEntity.radius;	
	}


	@Override
	public String toString() {
		return "Ball [x=" + x + ", y=" + y + ", z=" + z + ", velocity_x=" + velocity_x + ", velocity_y=" + velocity_y
				+ ", velocity_z=" + velocity_z + "]";
	}
	
	
}
