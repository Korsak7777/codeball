package strategy;


import java.lang.Math;

import math.SpeedVector;
import math.Vector2D;
import newModel.NewRobot;

public class PrimitiveMove{

	private NewRobot robot;
	
	public PrimitiveMove(NewRobot robot) {
		this.robot = robot;
	}

	public SpeedVector run(double mAX_ENTITY_SPEED, double x, double z) {
		double length = 
				Math.sqrt((x - robot.x)*(x - robot.x) + 
						(z - robot.z)*(z - robot.z));
		if (length==0) return new SpeedVector(0, 0, 0);
        double target_velocity_x = (x - robot.x)*mAX_ENTITY_SPEED/length;
        double target_velocity_z = (z - robot.z)*mAX_ENTITY_SPEED/length;

		return new SpeedVector(target_velocity_x, 0, target_velocity_z);
	}
	
	public SpeedVector run(double mAX_ENTITY_SPEED, Vector2D runPoint) {
		return run(mAX_ENTITY_SPEED, runPoint.getX(), runPoint.getY());
	}
	
	public SpeedVector run(Vector2D acceleration) {
		return new SpeedVector(acceleration.getX(), 0, acceleration.getY());
	}
	
	
}
