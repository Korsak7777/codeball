package strategy;


import math.Line;
import math.Vector1D;
import math.Vector2D;
import math.Vector3D;

import model.Game;
import model.Rules;
import newModel.NewBall;
import newModel.NewRobot;

public class HelpFun {

	private Rules rules;

	public HelpFun(Rules rules, Game game) {
		this.rules = rules;
	}

	public Vector2D ballXZCoord(NewBall ball) {
		return new Vector2D(ball.x, ball.z);
	}

	public Vector2D gatePosition() {
		return new Vector2D(0, (double)rules.arena.depth/2 + rules.arena.goal_depth/2);
	}
	public Vector2D myGatePosition2D() {
		return new Vector2D(0, -((double)rules.arena.depth/2 + rules.arena.goal_depth/4));
	}
	public Vector3D myGatePosition3D() {
		return new Vector3D(0, rules.BALL_RADIUS, -((double)rules.arena.depth/2 + rules.arena.goal_depth/2));
	}

	/**
	 * Возвращает позицию на линии соединяющей центр мяча и центр ворот
	 * distance - удаленность позиции от мяча (в плоскости арены)
	 * @param ball позиция мяча
	 * @param distance удаленность позиции от мяча (в плоскости арены)
	 * @return точку на земле
	 */
	public Vector2D kickPosition(NewBall ball, double distance) {
		Line line = new Line(gatePosition(), ballXZCoord(ball), rules.arena.goal_depth/2);

		Vector1D abscissa = line.toSubSpace(ballXZCoord(ball));
		if (abscissa.getNorm() == 0) return line.toSpace(abscissa);
		return line.toSpace(abscissa.add(abscissa.normalize().scalarMultiply(distance)));
	}

	public Vector2D findKickVector(NewBall ball) {
		return kickPosition(ball, 1);
	}

	public Vector2D kickBallPosition(NewRobot newRobot, NewBall ball, double distance) {
		Line line = new Line(newRobot.getPositionXZ(), ballXZCoord(ball), 0.25);
		Vector1D abscissa = line.toSubSpace(ballXZCoord(ball));
		if (abscissa.getNorm() == 0) return line.toSpace(abscissa);
		return line.toSpace(abscissa.add(abscissa.normalize().scalarMultiply(distance)));
	}
}
