package strategy;


import static java.lang.Math.atan;
import static java.lang.Math.cos;
import static java.lang.Math.hypot;
import static java.lang.Math.sin;


import model.Action;
import model.Arena;
import model.Ball;
import model.Game;
import model.Rules;
import newModel.NewRobot;

public class GoalkeeperStrategy{
	
	private Rules rules;
	private Game game;

	private Arena arena;
	private Ball ball;
	private Point ballPosition;

	private double MAX_ϕ_IN_THE_Goal_Area;
	private double GOAL_ARIA_RADIUS;
	
	GoalkeeperStrategy (Rules rules, Game game){
		this.rules = rules;
		this.game = game;
		arena = rules.arena;
		ball = game.ball;

		ballPosition = new Point(ball.x,ball.y,ball.z);

		// левая перекладина ворот(если смотреть из за наших ворот)
		Point goalLeftCrossbarPosition = new Point(arena.goal_width/2 - 4.0, arena.goal_height,-arena.depth/2.0);
		GOAL_ARIA_RADIUS = new Point(arena.goal_width/2.0 - 6.0, 0,-arena.depth/2.0).r();
		MAX_ϕ_IN_THE_Goal_Area = goalLeftCrossbarPosition.ϕ();
	}

	private Action moveToTheStampOnTheGround(Point stamp, NewRobot robot){
		Point robotPosition = new Point(robot.x,robot.y,robot.z);
		Point stampPositionByRobot = stamp.getPositionByNewPoinOfView(robotPosition);
		Action result = new Action();
		double currentVelocity = rules.ROBOT_ACCELERATION * Math.min(stampPositionByRobot.r(),arena.goal_width)/arena.goal_width;
		result.target_velocity_x = currentVelocity * stampPositionByRobot.sin_ϕ();
		result.target_velocity_z = currentVelocity * stampPositionByRobot.cos_ϕ();
		return result;
	}

	NewRobot runToGoalArea(NewRobot robot){
		double positionsϕ = Math.signum(ballPosition.ϕ()) * Math.min(Math.abs(ballPosition.ϕ()),MAX_ϕ_IN_THE_Goal_Area);
		robot.action = moveToTheStampOnTheGround(new Point(GOAL_ARIA_RADIUS,positionsϕ), robot);
		return robot;
	}
	
	class Point {
			
			public double x;
			public double y;
			public double z;
	
			public Point(double r,double ϕ){
				x = r * sin(ϕ);
				y = .0;
				z = r * cos(ϕ);
			}
	
			public Point(double x_ByMidfield,double y_ByMidfield,double z_ByMidfield){
				x = x_ByMidfield;
				y = y_ByMidfield;
				z = z_ByMidfield + arena.depth;//arena.depth;
			}
			public double r(){
				return hypot(hypot(x, y),z);
			}
			public double distanseTo(Point other){
				return other.getPositionByNewPoinOfView(this).r();
			}
			public Point getPositionByNewPoinOfView(Point newPointOfView){
				return new Point(x-newPointOfView.x,y-newPointOfView.y,z-newPointOfView.z-arena.depth);
			}
			
			public double[] toCoorinatsByMidfield(){
				return new double[] {x,y,z-arena.depth};
			}
			
			public double θ(){return atan(y/z) + (Math.signum(z)-1.)*Math.PI/2;}
			public double ϕ(){return atan(x/z) + (Math.signum(z)-1.)*Math.PI/2;}
			public double sin_ϕ(){return sin(ϕ());}
			public double cos_ϕ(){return cos(ϕ());}
	
		@Override
		public String toString() {
			return String.format("Point(x=%.2f; y=%.2f; z=%.2f; ϕ=%.2f°; sin(ϕ)=%.2f; cos(ϕ) = %.2f;",x,y,z,Math.toDegrees(ϕ()),sin_ϕ(),cos_ϕ());
		}
	}


}
