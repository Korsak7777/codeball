package strategy;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import math.ClampMath;
import math.Vector2D;
import math.Vector3D;

import model.Action;
import model.Game;
import model.Robot;
import model.Rules;
import newModel.NewBall;
import newModel.NewRobot;
import newModel.NewRules;

public class Analyzator {
    
	public static final double MAX_JUMP_HIGH = 3.765; //из гравитации и максимальной скорости прыжка

	private static volatile Analyzator instance; //делаем синглттоном
    public static Analyzator getAnalyzator(Rules rules, Game game) {
		Analyzator localInstance = instance;
		if (localInstance == null) {
			synchronized (Analyzator.class) {
				localInstance = instance;
				if (localInstance == null) {
					instance = localInstance = new Analyzator(rules, game);
				}
			}
		}
		return localInstance;
    }	

    private Rules rules;
	private Game game;
	
	private TeamStrategy teamStrategy;

	public double maxJumpHigh;

	private NewRules newRules;
	public void setGame(Game game) {
		this.game = game;
		this.mapBalls = futureBall(1, 80, 1);
		this.mapXZBalls = futureXZBall(1, 160, 1);
		this.preGoalBalls = pastBall(goalTick, 1);
		this.maxJumpHigh = MAX_JUMP_HIGH + rules.ROBOT_MIN_RADIUS;
		this.teamStrategy = new TeamStrategy(rules, game);
    	teamStrategy.initActions();	
    	isStartRun();
	}

	public TeamStrategy getTeamStrategy() {
		return teamStrategy;
	}

	private NewRobot allyRobot1, allyRobot2;
	public NewRobot getAllyRobot1() {
		return allyRobot1;
	}
	public NewRobot getAllyRobot2() {
		return allyRobot2;
	}
	public Map<Integer, Action> getRobotActions(){
        Map<Integer, Action> actions = new HashMap<>();
        actions.put(allyRobot1.id, allyRobot1.action);
        actions.put(allyRobot2.id, allyRobot2.action);
		return actions;
	}

	private Analyzator(Rules rules, Game game) {
		this.rules = rules;
		this.game = game;
		this.newRules = new NewRules(rules);
	}
	/**
	 * Выбирает роли роботам
	 * @param game
	 */
	void initRobot() { 
		List<NewRobot> enemies  = new ArrayList<NewRobot>(), alies = new ArrayList<NewRobot>();
		for (Robot robot : game.robots) {
        	if (robot.is_teammate) alies.add(new NewRobot(robot)); else enemies.add(new NewRobot(robot));
		}
		NewRobot allyRobot1, allyRobot2;
		allyRobot1 = alies.get(0); allyRobot2 = alies.get(1);

		this.allyRobot1 = allyRobot1; this.allyRobot2 = allyRobot2;
		
        //goalLogger();
	}

	private Map<Integer, NewBall> mapBalls;
	private Map<Integer, Vector2D> mapXZBalls;
	private NewBall goalBall;
	private int goalTick;
	private Map<Integer, NewBall> preGoalBalls;

	public Map<Integer, NewBall> getPreGoalBalls(){
		return preGoalBalls;
	}
	public Map<Integer, NewBall> getMapBalls() {
		return mapBalls;
	}
	public Map<Integer, Vector2D> getMapXZGoals() {
		return mapXZBalls;
	}
	/**
	 * @return мяч из будущего в наших воротах
	 */
	public NewBall getGoalBall() {
		return goalBall;
	}
	public int getGoalTick() {
		return goalTick;
	}
	/**
	 * Предсказывает будущие положения мяча начиная с startTick и заканчиваем finishTick
	 * с шагом step.
	 * @param startTick должен быть больше 0
	 * @param finishTick
	 * @param step
	 * @param physics
	 * @return корзину с предсказаниями: ключ - тик, начиная с начала игры; значение - мяч в этот тик
	 */
	private Map<Integer, NewBall> futureBall(int startTick, int finishTick, int step){
		Map<Integer, NewBall> futureBalls = new TreeMap<Integer, NewBall>();
		int current_tick = game.current_tick;
		NewBall ball2 = new NewBall(game.ball);
		for (; startTick < finishTick; ) {
			NewBall newBall = moveBall(ball2, startTick);
			
        	if (newBall.y < rules.arena.height &&
                    (Math.abs(newBall.x) < rules.arena.width/2.0) &&
                    (Math.abs(newBall.z) < rules.arena.depth/2.0) &&
                    (newBall.y > rules.BALL_RADIUS)) {
    			futureBalls.put(current_tick + startTick, newBall);//кладем его в нашу корзину предсказаний
    			startTick += step;
        	} else {
        		return futureBalls;
        	}
		}
		return futureBalls;		
	}

	/**
	 * показывает прошлое положения мяча начиная с finishTick и заканчиваем now
	 * с шагом step.
	 * @param finishTick
	 * @param step
	 * @param physics
	 * @return корзину с предсказаниями: ключ - тик, начиная с начала игры; значение - мяч в этот тик
	 */
	private Map<Integer, NewBall> pastBall(int finishTick, int step){ //гол на тике 100
		Map<Integer, NewBall> futureBalls = new TreeMap<Integer, NewBall>();
		int current_tick = game.current_tick;//сейчас тик 90
		if (goalBall != null) {
			NewBall ball2 = goalBall;
			finishTick = finishTick - current_tick;//10
			int i = 0;
			for (; current_tick < current_tick + finishTick; ) { //от 90 до 100
				NewBall newBall = moveBackBall(ball2, finishTick); //10(момент текущего мяча), 9, 8

	        	if (newBall.y < rules.arena.height &&
	                    (Math.abs(newBall.x) < rules.arena.width/2.0) &&
	                    (Math.abs(newBall.z) < rules.arena.depth/2.0 + rules.arena.goal_depth)) {
	    			futureBalls.put(current_tick + i, newBall);//кладем его в нашу корзину предсказаний
	    			i++;
	    			finishTick -= step;
	        	} else {
	        		return futureBalls;
	        	}
			}
		}
		return futureBalls;
	}

	/**
	 * Положение мяча из прошлого
	 * @param entity
	 * @param deltaTime 
	 * @return мяч на deltaTime раньше entity
	 */
	private NewBall moveBackBall(NewBall entity, double deltaTime) {
		Vector3D veloсity = ClampMath.clamp(entity.getVelosityPerTick(), newRules.MAX_ENTITY_SPEED_IN_TICKS);
		Vector3D position = entity.getPosition().subtract(veloсity.scalarMultiply(deltaTime));
        Vector3D gravitation = new Vector3D(0.0, newRules.GRAVITY_IN_TICKS, 0.0);

        NewBall finishEntity = new NewBall(position.add(gravitation.scalarMultiply(deltaTime*deltaTime/2.0)),
        		entity.getVelosity().add(gravitation.scalarMultiply(rules.TICKS_PER_SECOND*deltaTime)), entity);

		return finishEntity;
	}
	/**
	 * Предсказывает положение мяча через deltaTime. Не учитывает столкновение с поверхностью
	 * @param entity
	 * @param deltaTime
	 * @return
	 */
	private NewBall moveBall(NewBall entity, double deltaTime) {
		Vector3D veloсity = ClampMath.clamp(entity.getVelosityPerTick(), newRules.MAX_ENTITY_SPEED_IN_TICKS);
		Vector3D position = entity.getPosition().add(veloсity.scalarMultiply(deltaTime));
        Vector3D gravitation = new Vector3D(0.0, newRules.GRAVITY_IN_TICKS, 0.0);

        NewBall finishEntity = new NewBall(position.subtract(gravitation.scalarMultiply(deltaTime*deltaTime/2.0)),
        		entity.getVelosity().subtract(gravitation.scalarMultiply(rules.TICKS_PER_SECOND*deltaTime)), entity);

		return finishEntity;
	}
	private Map<Integer, Vector2D> futureXZBall(int startTick, int finishTick, int step){
		Map<Integer, Vector2D> futureBalls = new TreeMap<Integer, Vector2D>();
		int current_tick = game.current_tick;
		NewBall ball2 = new NewBall(game.ball);
		for (; startTick < finishTick; ) {
			Vector2D newBall = moveBallXZ(ball2, startTick);
        	if ((Math.abs(newBall.getX()) < rules.arena.width/2.0) &&
                    (Math.abs(newBall.getY()) < rules.arena.depth/2.0 + rules.arena.goal_depth)) {
    			futureBalls.put(current_tick + startTick, newBall);//кладем его в нашу корзину предсказаний
    			if (newBall.getY() < -rules.arena.depth/2.0 & Math.abs(newBall.getX())<rules.arena.goal_width/2.0) {
    				NewBall newBall2 = moveBall(ball2, startTick);
    				if (newBall2.y<(rules.arena.goal_height-rules.BALL_RADIUS/2)) {
    					goalBall = newBall2;
    					goalTick = current_tick + startTick;
                		return futureBalls;
    				}
    				goalBall = null;
            		return futureBalls;
    			}
    			startTick += step;
        	} else {
        		return futureBalls;
        	}
		}
		return futureBalls;		
	}
	private Vector2D moveBallXZ(NewBall entity, double deltaTime) {
		Vector2D velocityXZ = entity.getVelosityXZperTick().scalarMultiply(deltaTime);
		Vector2D position = entity.getPositionXZ().add(velocityXZ);

		return position;
	}
	
	Map<Integer, NewRobot> futureJumpRobot(NewRobot entity, double jumpSpeed, int finishTick, int step){
		Map<Integer, NewRobot> futureBalls = new TreeMap<Integer, NewRobot>();
		int current_tick = game.current_tick;
		int startTick = game.current_tick;
		for (; startTick < finishTick; ) {
			NewRobot newBall = jumpRobot(entity, jumpSpeed, startTick);
			
        	if (newBall.y < rules.arena.height &&
                    (Math.abs(newBall.x) < rules.arena.width/2.0) &&
                    (Math.abs(newBall.z) < rules.arena.depth/2.0) &&
                    (newBall.y > rules.BALL_RADIUS)) {
    			futureBalls.put(current_tick + startTick, newBall);//кладем его в нашу корзину предсказаний
    			startTick += step;
        	} else {
        		return futureBalls;
        	}
		}
		return futureBalls;		
	}
	
	/**
	 * @param entity
	 * @param jumpSpeed в еденицах метр/тик
	 * @param deltaTime
	 * @return
	 */
	private NewRobot jumpRobot(NewRobot entity, double jumpSpeed, double deltaTime) {
		Vector3D veloсity = ClampMath.clamp(entity.getVelosityPerTick().
				add(new Vector3D(0.0, jumpSpeed, 0.0)), newRules.MAX_ENTITY_SPEED_IN_TICKS);
//		double deltaTime = jumpSpeed/newRules.GRAVITY_IN_TICKS * 2.0;

		Vector3D position = entity.getPosition().add(veloсity.scalarMultiply(deltaTime));
        Vector3D gravitation = new Vector3D(0.0, newRules.GRAVITY_IN_TICKS, 0.0);

        NewRobot finishEntity = new NewRobot(position.subtract(gravitation.scalarMultiply(deltaTime*deltaTime/2.0)),
        		entity.getVelosity().subtract(gravitation.scalarMultiply(rules.TICKS_PER_SECOND*deltaTime)), entity);

		return finishEntity;
	}
	
	boolean isStartRun = true;
	private void isStartRun() {
		if (game.ball.x == 0 && game.ball.z == 0 
				&& game.ball.velocity_x == 0 && game.ball.velocity_z == 0 && game.ball.velocity_y ==0 
				&& game.ball.y != ballStartY) {
			
			ballStartY = game.ball.y;
			
			if (game.ball.y > 3.57 & game.ball.y < 5.4) {
				System.out.println("isStartRun game.ball.y " + game.ball.y);
				isStartRun = false;
			}
		}
	}
	
	private int score = 0;
	private int lastGoalTime = 0;
	private double ballStartY = 0;
	private Vector2D ballRobot1Dist = new Vector2D(0, 0);
	private Vector2D ballRobot2Dist = new Vector2D(0, 0);
	private void goalLogger() {
		int currentScore = game.players[0].score;
		
		if ( score != currentScore) {
			score = currentScore;
			int deltaGoalTime = - lastGoalTime + game.current_tick;
			System.out.println("Гол забили за " + deltaGoalTime + " тиков при мяча: " + ballStartY +
					" робот1: " + ballRobot1Dist + " робот2: " + ballRobot2Dist);
			System.out.println("Счет: " + currentScore);
		}
		
		if (game.ball.x == 0 && game.ball.z == 0 
				&& game.ball.velocity_x == 0 && game.ball.velocity_z == 0 && game.ball.velocity_y ==0 
				&& game.ball.y != ballStartY) {
			
			System.out.println("Положение мяча: " + game.ball.y +
					" робот1: " + ballRobot1Dist.toString() + " робот2: " + ballRobot2Dist.toString());
			
			ballStartY = game.ball.y;
			ballRobot1Dist = new Vector2D(game.robots[0].x, game.robots[0].z);
			ballRobot2Dist = new Vector2D(game.robots[1].x, game.robots[1].z);
			lastGoalTime = game.current_tick;
		}

		if (game.ball.y > 4.765) {
			System.out.println("currentTick: " + game.current_tick + " currentBall: " + game.ball);
			System.out.println("currentVelocity: " + new NewBall(game.ball).getVelosity().scalarMultiply((double)1/rules.TICKS_PER_SECOND).toString());

			futureBall(1, 3, 1);
		}
	}

	
	
}
