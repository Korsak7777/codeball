package strategy;


import model.Game;
import model.Rules;
import newModel.NewBall;
import newModel.NewRobot;

public class TeamStrategy {
	
	private Rules rules;
	private Game game;			

	private Analyzator analyzator = Analyzator.getAnalyzator(rules, game);
	private HelpFun helpFun;
	private SmartKickBall smartKickBallDefence;
	private SmartKickBall smartKickBallAtack;

	
	private NewRobot robot1, robot2;
		
	public NewRobot getRobot1() {
		return robot1;
	}

	public NewRobot getRobot2() {
		return robot2;
	}

	public TeamStrategy(Rules rules, Game game) {
		this.rules = rules;
		this.game = game;
				
		this.helpFun = new HelpFun(rules, game);
		this.smartKickBallDefence = new SmartKickBall(rules, game);
		this.smartKickBallAtack = new SmartKickBall(rules, game);
	}
	
	public void initActions() {
		analyzator.initRobot(); //определяем роботов
		
		robot1 = analyzator.getAllyRobot1(); robot2 = analyzator.getAllyRobot2();

		double distanceGateRobot1 = robot1.getPositionXZ().distance(helpFun.myGatePosition2D());
		double distanceGateRobot2 = robot2.getPositionXZ().distance(helpFun.myGatePosition2D());

		if (distanceGateRobot1 > distanceGateRobot2) { //кто ближе к воротам тот и вратарь
			robot1.action = smartKickBallAtack.smartKickBallAction(robot1).action;
			robot2.action = smartKickBallDefence.smartDefenceAction(robot2).action;	
		} else {
			robot2.action = smartKickBallAtack.smartKickBallAction(robot2).action;
			robot1.action = smartKickBallDefence.smartDefenceAction(robot1).action;
		}
	}
	

		

}
