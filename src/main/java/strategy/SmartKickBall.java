package strategy;


import java.util.Map;
import java.util.Map.Entry;

import math.Vector2D;

import model.Game;
import model.Rules;
import newModel.NewBall;
import newModel.NewRobot;
import newModel.NewRules;

public class SmartKickBall {

	private static final double MAX_JUMP_HIGH = 3.765; //из гравитации и максимальной скорости прыжка

	private Rules rules;
	private NewRules newRules;
	private Game game;
	private HelpFun helpFun;
	private Analyzator analyzator = Analyzator.getAnalyzator(rules, game);

	public SmartKickBall(Rules rules, Game game) {
		this.rules = rules;
		this.game = game;
				
		this.newRules = new NewRules(rules);
		this.helpFun = new HelpFun(rules, game);
		this.ball = new NewBall(game.ball);
	}
	
	private final NewBall ball;
	
	private NewBall goalBall;
	private double robotBallMinDist = 1000.0;
	private int robotBallTick;
	private void crossPoint(NewRobot robot) {
		Map<Integer, NewBall> preGoalBalls = analyzator.getPreGoalBalls();

		for (Entry<Integer, NewBall> pre : preGoalBalls.entrySet()){ //найти точку где перехватим
			NewBall ball = pre.getValue();
			double d = ball.getPositionXZ().distance(robot.getPositionXZ());

			if (ball.y < (analyzator.maxJumpHigh + 1) && d < robotBallMinDist && //точка перехвата - минимальная дстанция
					ball.z > -(rules.arena.depth/2.0+rules.BALL_RADIUS) && Math.abs(ball.x) < rules.arena.goal_width/2.0) {
				robotBallMinDist = d; robotBallTick = pre.getKey().intValue(); goalBall = ball;
			}
		}
	}
	private void crossPointHG(NewRobot robot) {
		Map<Integer, NewBall> preGoalBalls = analyzator.getPreGoalBalls();

		for (Entry<Integer, NewBall> pre : preGoalBalls.entrySet()){ //найти точку где перехватим
			NewBall ball = pre.getValue();
			double d = ball.getPositionXZ().distance(robot.getPositionXZ());

			if (ball.y < (analyzator.maxJumpHigh + rules.BALL_RADIUS - 0.5) && d < robotBallMinDist && //точка перехвата - минимальная дстанция
					ball.z > -(rules.arena.depth/2.0+rules.BALL_RADIUS) && Math.abs(ball.x) < rules.arena.goal_width/2.0) {
				robotBallMinDist = d; robotBallTick = pre.getKey().intValue(); goalBall = ball;
			}
		}
	}

	private boolean isJumpRobotCrossBall(NewRobot robot, NewBall goalBall, double robotJumpVelocityY, double halfFlyTick) {
		Map<Integer, NewRobot> futureJumpRobots = analyzator.futureJumpRobot(robot, robotJumpVelocityY, (int) halfFlyTick, 1);
		for (Entry<Integer, NewRobot> fu : futureJumpRobots.entrySet()){ //найти точку где столкнуться
			Vector2D robotXZ = fu.getValue().getPositionXZ();
			double crossDist = rules.BALL_RADIUS + rules.ROBOT_MIN_RADIUS;
			
			if (Math.abs((robotXZ.getX()-goalBall.x))<crossDist && Math.abs((robotXZ.getY()-goalBall.z))<crossDist) return true;
		}
		return false;
	}
	
	private NewRobot goalDefence(NewRobot robot) {
		PrimitiveMove primitiveMove = new PrimitiveMove(robot);

		crossPoint(robot);
		
		if (goalBall == null) crossPointHG(robot);
		
//		System.out.println("Tик: " + game.current_tick);
		
		if (goalBall == null) {
			goalBall = new NewBall(game.ball);
//			System.out.println("goalBall: " + goalBall);
		}

		double robotJumpVelocityY = Math.sqrt(2.0*newRules.GRAVITY_IN_TICKS*goalBall.y);
		double halfFlyTick = robotJumpVelocityY/newRules.GRAVITY_IN_TICKS;
		double halfJumpDistance = robot.getVelosityXZperTick().getNorm() * halfFlyTick;

		double jumpVelocity = 2.0*robotBallMinDist/(double)(robotBallTick-game.current_tick);
		
		if (jumpVelocity > rules.ROBOT_ACCELERATION) jumpVelocity = rules.ROBOT_ACCELERATION;

		if (halfJumpDistance > robotBallMinDist
				&& isJumpRobotCrossBall(robot, goalBall, robotJumpVelocityY, halfFlyTick)
			) {
//			System.out.println("robot.getPositionXZ: " + robot.getPositionXZ());
//			System.out.println("goalBall.getPositionXZ(): " + goalBall.getPositionXZ());
//			System.out.println("jumpVelocity: " + jumpVelocity + " robotJumpVelocityY: " + robotJumpVelocityY);
			robot.action = primitiveMove.run(jumpVelocity, goalBall.getPositionXZ()).toAction();
			robot.action.jump_speed = robotJumpVelocityY * rules.TICKS_PER_SECOND;
			return robot;
		} else if(halfJumpDistance > robotBallMinDist) {
			return kickBallAction(robot, ball);
		}
//		System.out.println("robot.getPositionXZ: " + robot.getPositionXZ());
//		System.out.println("goalBall.getPositionXZ(): " + goalBall.getPositionXZ());
		robot.action = primitiveMove.run(jumpVelocity * rules.TICKS_PER_SECOND, goalBall.getPositionXZ()).toAction();
		return robot;	
	}
	

	NewRobot smartDefenceAction(NewRobot robot) {
		if (ball.x == 0 && ball.z == 0 
				&& ball.velocity_x == 0 && ball.velocity_z == 0 && analyzator.isStartRun) {
			return kickBallAction(robot, ball);
		}
		if (ball.getPosition().subtract(helpFun.myGatePosition3D()).getNorm() > 35) {
			GoalkeeperStrategy goalkeeperStrategy = new GoalkeeperStrategy(rules, game);
			return goalkeeperStrategy.runToGoalArea(robot);
		}
		if (analyzator.getGoalBall() != null && !analyzator.getPreGoalBalls().isEmpty()) {//если мяч летит в наши ворота
			return goalDefence(robot);
		}
		GoalkeeperStrategy goalkeeperStrategy = new GoalkeeperStrategy(rules, game);
		return goalkeeperStrategy.runToGoalArea(robot);	
	}
	
//	NewRobot defenceAction(NewRobot robot) {
//		PrimitiveMove primitiveMove = new PrimitiveMove(robot);
//		NewBall ball = new NewBall(game.ball);
//		if (ball.getPosition().subtract(helpFun.myGatePosition3D()).getNorm() > 45) {
//			robot.action = primitiveMove.run(rules.ROBOT_ACCELERATION, helpFun.myGatePosition2D()).toAction();
//			return robot;
//		}
//		return kickBallAction(robot, ball);
//	}
	
	private double robotAtackBallMinDist;
	private int robotAttackBallTick;
	private NewBall atackBall;
	private void crossBall(NewRobot robot) {
		Map<Integer, NewBall> futureBalls = analyzator.getMapBalls();
		for (Entry<Integer, NewBall> fu : futureBalls.entrySet()){ //найти точку где перехватим		
			NewBall ball = fu.getValue();
			double d = ball.getPositionXZ().distance(robot.getPositionXZ());
			if (ball.y < (analyzator.maxJumpHigh + 1) && d < robotBallMinDist && //точка перехвата - минимальная дстанция
					robot.getZ() - 1.0 < ball.getZ()) {
				robotAtackBallMinDist = d; robotAttackBallTick = fu.getKey().intValue(); atackBall = ball;
			}
		}
	}
	
	NewRobot smartKickBallAction(NewRobot robot) {
		PrimitiveMove primitiveMove = new PrimitiveMove(robot);
		if (analyzator.getGoalBall() != null && !analyzator.getPreGoalBalls().isEmpty()) {//если мяч летит в наши ворота
			return goalDefence(robot);
		} else {
			crossBall(robot);
			if(atackBall != null) {
				double robotJumpVelocityY = Math.sqrt(2.0*newRules.GRAVITY_IN_TICKS*atackBall.y);
				double halfFlyTick = robotJumpVelocityY/newRules.GRAVITY_IN_TICKS;
				double halfJumpDistance = robot.getVelosityXZperTick().getNorm() * halfFlyTick;
 
				double jumpVelocity = 2.0*robotAtackBallMinDist/(double)(robotAttackBallTick-game.current_tick);
				
				Vector2D kickPosition = helpFun.kickPosition(ball, halfJumpDistance);	
				
				if (jumpVelocity > rules.ROBOT_ACCELERATION) jumpVelocity = rules.ROBOT_ACCELERATION;

				if (halfJumpDistance > robotAtackBallMinDist
						&& isJumpRobotCrossBall(robot, atackBall, robotJumpVelocityY, halfFlyTick)
					) {
//					System.out.println("robot.getPositionXZ: " + robot.getPositionXZ());
//					System.out.println("goalBall.getPositionXZ(): " + goalBall.getPositionXZ());
//					System.out.println("jumpVelocity: " + jumpVelocity + " robotJumpVelocityY: " + robotJumpVelocityY);
					robot.action = primitiveMove.run(jumpVelocity, atackBall.getPositionXZ()).toAction();
					robot.action.jump_speed = robotJumpVelocityY * rules.TICKS_PER_SECOND;
					return robot;
				} else if(halfJumpDistance > robotAtackBallMinDist) {
					return kickBallAction(robot, ball);
				}
//				System.out.println("robot.getPositionXZ: " + robot.getPositionXZ());
//				System.out.println("goalBall.getPositionXZ(): " + goalBall.getPositionXZ());
				robot.action = primitiveMove.run(jumpVelocity * rules.TICKS_PER_SECOND, kickPosition).toAction();
				return robot;	
			}
		}
		return kickBallAction(robot, ball);
	}


	NewRobot kickBallAction(NewRobot robot, NewBall ball) {
		PrimitiveMove primitiveMove = new PrimitiveMove(robot);

		double maxJumpHigh = MAX_JUMP_HIGH + rules.ROBOT_MIN_RADIUS;

		double distanceXZBallRobot = robot.getPositionXZ().distance(ball.getPositionXZ());

		if(robot.getZ() - 1.0 < ball.getZ() & ball.y < maxJumpHigh &
				distanceXZBallRobot < rules.BALL_RADIUS + rules.ROBOT_MIN_RADIUS * 2.0) {
			robot.action = primitiveMove.run(rules.ROBOT_ACCELERATION, helpFun.kickPosition(ball, 0)).toAction();
			robot.action.jump_speed = rules.ROBOT_MAX_JUMP_SPEED;
			return robot;
		} else if(ball.y < maxJumpHigh & ball.z < robot.z) { //огибает препятствие
			Vector2D point = new Vector2D(ball.x, ball.z);			
			Vector2D newPoint = new Vector2D(point.getX() - Math.signum(point.getX()) * 3.0 * ball.radius, point.getY());
			robot.action = primitiveMove.run(rules.ROBOT_ACCELERATION, newPoint).toAction();
			return robot;
		}
		Vector2D kickPosition = helpFun.kickPosition(ball, rules.BALL_RADIUS);

		robot.action = primitiveMove.run(rules.ROBOT_ACCELERATION,kickPosition).toAction();

		return robot;
	}
}
