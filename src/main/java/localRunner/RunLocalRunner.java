package localRunner;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RunLocalRunner {
	//private static final Logger logger = LogManager.getLogger(RunLocalRunner.class);

	private static Process process;
	
	private static List<String> getLocalRunerArgs(boolean isTest, String os){
    	List<String> args = new ArrayList<>();
    	
    	args.add("src/test/resources/codeball2018-"+os);
    	args.add("--team-size"); args.add("2");
    	args.add("--p1"); args.add("tcp-31001");
    	args.add("--p2"); 
		if (isTest) args.add("tcp-31002"); else args.add("helper");
		args.add("--nitro"); args.add("false");
		args.add("--fast-forward"); args.add("0");
		args.add("--no-countdown");
//		logger.trace(args);
		return args;
    }
    
	private static void runLinux(boolean isTest) throws IOException {
		runOnSystem(getLocalRunerArgs(isTest, "linux/codeball2018"));		
	}	
	
	private static void runWindows(boolean isTest) throws IOException {
		runOnSystem(getLocalRunerArgs(isTest, "windows/codeball2018"));
	}
	private static void runMac(boolean isTest) throws IOException {
		runOnSystem(getLocalRunerArgs(isTest, "macos/codeball2018"));
	}
	
	private static void runOnSystem(List<String> args) throws IOException {
		Process process = new ProcessBuilder(args).start();
//		logger.trace("Process " + process.isAlive());
	}
		
	private static String defineSystem() {
//		logger.trace( System.getProperty("os.name"));
		return System.getProperty("os.name");		
	}	
	/**
	 * запускает Локал Раннер
	 * @throws IOException 
	 */
	/**
	 * запускает Локал Раннер
	 * @param isTest true если это unit тест
	 * @throws IOException
	 */
	public static void run(boolean isTest) throws IOException {
		switch (defineSystem()) {
		case "Linux":
			runLinux(isTest); break;
		case "Windows 7":
			runWindows(isTest); break;			
		case "Windows 8.1":
			runWindows(isTest); break;
		case "Mac OS X":
			runMac(isTest); break;
		default:
//			logger.fatal("Ты кто блэд?"); break;
		}
	}
	
	public static void close() throws IOException {
		process.destroy();
	}
}
