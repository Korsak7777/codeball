import model.*;
import newModel.NewRobot;
import strategy.Analyzator;
import strategy.TeamStrategy;

public final class MyStrategy implements Strategy {
        
	Analyzator analyzator;
    TeamStrategy teamStrategy;
    
    int tick = 0;
    
    Rules rules;
    Game game;

    private void init() {
    	analyzator = Analyzator.getAnalyzator(rules, game);
    	analyzator.setGame(game);
    	teamStrategy = analyzator.getTeamStrategy();
    	tick++;
    }

    @Override
    public void act(Robot me, Rules rules, Game game, Action action) {  
    	this.rules = rules; this.game = game;

    	if (tick == game.current_tick) init();

    	NewRobot robot1 = teamStrategy.getRobot1();
    	NewRobot robot2 = teamStrategy.getRobot2();
    	Action action1 = new Action();
    	if (me.id == robot1.id)	action1 = robot1.action;
    	if (me.id == robot2.id)	action1 = robot2.action;

    	action.target_velocity_x = action1.target_velocity_x;
    	action.target_velocity_y = action1.target_velocity_y;
    	action.target_velocity_z = action1.target_velocity_z;
    	action.jump_speed = action1.jump_speed;
    	action.use_nitro = false;
    }

    @Override
    public String customRendering() {
        return "";
    }
}
